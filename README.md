# README #

Hello! My name is Andrew and I am aiming to be Java developer.

### What is this repository for? ###

The purpose of this repository is to track my progress in learning Java. 
I will regularly upload completed exercises and small educational projects.

### About me ###

* I am 28 years old.
* I do not have computer science or IT degree, or any IT education experience, but I have degree in radio frequency engineering.
* I am from Russia, Saint-Petersburg.
* I decided to become programmer after working on my own small project about russian language. For that purpose I used PHP. Not only for web-part, but also for scripting and linguistics.

### Contribution  ###

Code Reviewing is needed. Show me my mistakes!
